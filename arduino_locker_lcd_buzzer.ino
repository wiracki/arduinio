#include <Password.h> 
#include <Keypad.h> 
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#define BACKLIGHT_PIN 3

const int buzzer = 2; 
int guzik =0;
int stan;
boolean sw =false;

const byte ROWS = 4; 
const byte COLS = 4; 

char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte rowPins[ROWS] = { 9, 8, 7, 6 };
byte colPins[COLS] = { 13, 12, 11, 10 }; 

LiquidCrystal_I2C	lcd(0x27,2,1,0,4,5,6,7);

Password password = Password("1234");

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

void setup(){
  pinMode(buzzer, OUTPUT);
  pinMode(guzik, INPUT_PULLUP);
  Serial.begin(9600);
  lcd.begin (16,2);
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.setBacklight(LOW);
  lcd.home();
  lcd.print("WPISZ HASLO:");
  lcd.setCursor(0,1); 
  keypad.addEventListener(keypadEvent);
    
}

void cls() {
  lcd.clear();
  lcd.home();
  lcd.print("WPISZ HASLO:");
  lcd.setCursor(0,1); }

void loop(){
   int jasn = analogRead(A0);
   if (jasn<600) {lcd.setBacklight(HIGH);}
   else {
   stan = digitalRead(guzik);
   delay(100);
   if (stan==0){ sw = !sw ; lcd.setBacklight(sw);} }
   keypad.getKey();
  
}

void keypadEvent(KeypadEvent eKey){
  switch (keypad.getState()){
    case PRESSED:
	Serial.print("Pressed: ");
	Serial.println(eKey);
        lcd.print('*');
	switch (eKey){
	  case '*': checkPassword(); break;
	  case '#': password.reset(); cls(); break;
	  default: password.append(eKey);
     }
  }
}

void checkPassword(){
  if (password.evaluate()){
    correctPassword();
  }else{
   wrongPassword();
  }
}

void correctPassword(){
    cls();
    Serial.println("POPRAWNE");
    lcd.print("POPRAWNE");
    tone(buzzer, 250);
    delay(500);
    tone(buzzer,500);
    delay(500);
    tone(buzzer, 250);
    delay(1000);
    noTone(buzzer);
    cls();
}

void wrongPassword(){
 cls();
    Serial.println("ZLE");
    lcd.print("ZLE");
    password.reset();
    tone(buzzer, 1000);
    delay(2000);
    cls();
    noTone(buzzer);
}

